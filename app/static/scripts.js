function openURL(url, target = '_self') {
	window.open(url, target);
}

for (var navButton of document.getElementsByClassName('nav__button')) {
	navButton.classList.remove('nav__button--active');
}
document.getElementById(window.location.pathname).classList.add('nav__button--active');